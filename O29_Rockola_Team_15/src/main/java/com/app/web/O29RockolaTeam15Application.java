package com.app.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class O29RockolaTeam15Application {

	public static void main(String[] args) {
		SpringApplication.run(O29RockolaTeam15Application.class, args);
	}

}
