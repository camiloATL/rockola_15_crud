package com.app.web.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.app.web.entidades.Escuchar;
import com.app.web.entidades.Usuario;
import com.app.web.servicios.UsuarioServicio;

@Controller
public class UsuarioControl {
	
	@Autowired
	private UsuarioServicio servicio;
	
	//PETICION LISTAR
	@GetMapping({"/usuario/lista","/"})
	public String listarUsuario(Model modelo) {
			modelo.addAttribute("usuarios", servicio.listarUsuarios());
		return "/usuario/lista";
	}
	
	//PETICION MOSTRAR EL FORMULARIO
	@GetMapping("/usuario/crear")
	public String formularioCrear(Model modelo) {
		Usuario objUsuario = new Usuario();
		Escuchar objEscuchar = new Escuchar();
		modelo.addAttribute("usuario", objUsuario);
		modelo.addAttribute("escuchar", objEscuchar);
		return "/usuario/crear";
	}
	
	//PETICION PARA GUARDAD FORMULARIO
		@PostMapping("/usuario/lista")
		public String guardarUsuario(@ModelAttribute("usuario") Usuario objUsuario) {
			servicio.guardarUsuario(objUsuario);
			return "redirect:/usuario/lista";
		}
		
		
		//METODO PARA MOSTRAR EL FORMULARIO A EDITAR
		@GetMapping("/usuario/editar/{id}")
		public String formularioEditar(@PathVariable int id, Model modelo) {
			modelo.addAttribute("usuario", servicio.usuarioXid(id));
			return "/usuario/editar";
		}
		
		//PETICION PARA ACTUALIZAR EL USUARIO
		@PostMapping("/usuario/lista/{id}")
		public String editarUsuario(@PathVariable int id,
				@ModelAttribute("usuario") Usuario objUsuario){
			Usuario usuarioExistente = servicio.usuarioXid(id);
			usuarioExistente.setCedulaUsuario(objUsuario.getCedulaUsuario());
			usuarioExistente.setNombreUsuario(objUsuario.getNombreUsuario());
			usuarioExistente.setApellidoUsuario(objUsuario.getApellidoUsuario());
			usuarioExistente.setEdadUsuario(objUsuario.getEdadUsuario());
			usuarioExistente.setCorreoUsuario(objUsuario.getCorreoUsuario());
			usuarioExistente.setContrasenaUsuario(objUsuario.getContrasenaUsuario());
			
			servicio.actualizarUsuario(usuarioExistente);
			return "redirect:/usuario/lista";
			
		}
		
		@GetMapping("/usuario/eliminar/{id}")
		public String borrarUsuario(@PathVariable int id) {
			servicio.eliminarUsuario(id);
			return "redirect:/usuario/lista";
		}
		
		//peticion mostrar Home
		@GetMapping("/home/homeRockola")
		public String mostrarHome() {
			return null;
			
		}
}
