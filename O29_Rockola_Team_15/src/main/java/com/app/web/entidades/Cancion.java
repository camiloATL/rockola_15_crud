package com.app.web.entidades;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="canciones")
public class Cancion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	private int idCancion;
	
	@Column(name="nombreCancion", nullable = true, length = 100)
	private String nombreCancion;
	
	@Column(name="duracionCancion", nullable = true, length = 100)
	private int duracionCancion;
	
	@ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="idArtistaFK", referencedColumnName="idArtista")
	private Artista cancionArtista;
	
	@OneToMany(mappedBy="escucharCancion")
	private Set<Escuchar> escucucharCanciones;

	public Cancion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cancion(int idCancion, String nombreCancion, int duracionCancion, Artista cancionArtista,
			Set<Escuchar> escucucharCanciones) {
		super();
		this.idCancion = idCancion;
		this.nombreCancion = nombreCancion;
		this.duracionCancion = duracionCancion;
		this.cancionArtista = cancionArtista;
		this.escucucharCanciones = escucucharCanciones;
	}

	public int getIdCancion() {
		return idCancion;
	}

	public void setIdCancion(int idCancion) {
		this.idCancion = idCancion;
	}

	public String getNombreCancion() {
		return nombreCancion;
	}

	public void setNombreCancion(String nombreCancion) {
		this.nombreCancion = nombreCancion;
	}

	public int getDuracionCancion() {
		return duracionCancion;
	}

	public void setDuracionCancion(int duracionCancion) {
		this.duracionCancion = duracionCancion;
	}

	public Artista getCancionArtista() {
		return cancionArtista;
	}

	public void setCancionArtista(Artista cancionArtista) {
		this.cancionArtista = cancionArtista;
	}

	public Set<Escuchar> getEscucucharCanciones() {
		return escucucharCanciones;
	}

	public void setEscucucharCanciones(Set<Escuchar> escucucharCanciones) {
		this.escucucharCanciones = escucucharCanciones;
	}

	@Override
	public String toString() {
		return "Cancion [idCancion=" + idCancion + ", nombreCancion=" + nombreCancion + ", duracionCancion="
				+ duracionCancion + ", cancionArtista=" + cancionArtista + ", escucucharCanciones="
				+ escucucharCanciones + "]";
	}
	
	
}
