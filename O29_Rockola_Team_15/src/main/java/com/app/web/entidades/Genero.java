package com.app.web.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="generos")
public class Genero {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int idGenero;
	
	@Column(name="nombreGenero", nullable = false, length = 20 ) 
	private String nombreGenero;
	
	@OneToOne(mappedBy="generoUsuario")
	private Artista generoUsuario;

	public Genero() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Genero(int idGenero, String nombreGenero, Artista generoUsuario) {
		super();
		this.idGenero = idGenero;
		this.nombreGenero = nombreGenero;
		this.generoUsuario = generoUsuario;
	}

	public int getIdGenero() {
		return idGenero;
	}

	public void setIdGenero(int idGenero) {
		this.idGenero = idGenero;
	}

	public String getNombreGenero() {
		return nombreGenero;
	}

	public void setNombreGenero(String nombreGenero) {
		this.nombreGenero = nombreGenero;
	}

	public Artista getGeneroUsuario() {
		return generoUsuario;
	}

	public void setGeneroUsuario(Artista generoUsuario) {
		this.generoUsuario = generoUsuario;
	}

	@Override
	public String toString() {
		return "Genero [idGenero=" + idGenero + ", nombreGenero=" + nombreGenero + ", generoUsuario=" + generoUsuario
				+ "]";
	}
	
	
}
