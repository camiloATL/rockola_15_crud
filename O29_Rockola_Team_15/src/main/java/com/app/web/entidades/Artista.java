package com.app.web.entidades;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="artistas")
public class Artista {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	private int idArtista;
	
	@Column(name="nombreArtistico", nullable = true, length = 100)
	private String nombreArtistico;
	
	@OneToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name="idGeneroFK", referencedColumnName="idGenero")
	private Genero generoUsuario;
	
	@OneToMany(mappedBy="cancionArtista")
	private Set<Cancion> cancionesArtista;

	public Artista() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Artista(int idArtista, String nombreArtistico, Genero generoUsuario, Set<Cancion> cancionesArtista) {
		super();
		this.idArtista = idArtista;
		this.nombreArtistico = nombreArtistico;
		this.generoUsuario = generoUsuario;
		this.cancionesArtista = cancionesArtista;
	}

	public int getIdArtista() {
		return idArtista;
	}

	public void setIdArtista(int idArtista) {
		this.idArtista = idArtista;
	}

	public String getNombreArtistico() {
		return nombreArtistico;
	}

	public void setNombreArtistico(String nombreArtistico) {
		this.nombreArtistico = nombreArtistico;
	}

	public Genero getGeneroUsuario() {
		return generoUsuario;
	}

	public void setGeneroUsuario(Genero generoUsuario) {
		this.generoUsuario = generoUsuario;
	}

	public Set<Cancion> getCancionesArtista() {
		return cancionesArtista;
	}

	public void setCancionesArtista(Set<Cancion> cancionesArtista) {
		this.cancionesArtista = cancionesArtista;
	}

	@Override
	public String toString() {
		return "Artista [idArtista=" + idArtista + ", nombreArtistico=" + nombreArtistico + ", generoUsuario="
				+ generoUsuario + ", cancionesArtista=" + cancionesArtista + "]";
	}
	
	
}
