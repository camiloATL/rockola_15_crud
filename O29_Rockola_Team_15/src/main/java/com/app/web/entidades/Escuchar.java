package com.app.web.entidades;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="Escuchar")
public class Escuchar {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	private int idEscuchar;
	
	@ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="idUsuarioFK", referencedColumnName="idUsuario")
	private Usuario oyeUsuario;
	
	@ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="idCancionFK", referencedColumnName="idCancion")
	private Cancion escucharCancion;

	public Escuchar() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Escuchar(int idEscuchar, Usuario oyeUsuario, Cancion escucharCancion) {
		super();
		this.idEscuchar = idEscuchar;
		this.oyeUsuario = oyeUsuario;
		this.escucharCancion = escucharCancion;
	}

	public int getIdEscuchar() {
		return idEscuchar;
	}

	public void setIdEscuchar(int idEscuchar) {
		this.idEscuchar = idEscuchar;
	}

	public Usuario getOyeUsuario() {
		return oyeUsuario;
	}

	public void setOyeUsuario(Usuario oyeUsuario) {
		this.oyeUsuario = oyeUsuario;
	}

	public Cancion getEscucharCancion() {
		return escucharCancion;
	}

	public void setEscucharCancion(Cancion escucharCancion) {
		this.escucharCancion = escucharCancion;
	}

	@Override
	public String toString() {
		return "Escuchar [idEscuchar=" + idEscuchar + ", oyeUsuario=" + oyeUsuario + ", escucharCancion="
				+ escucharCancion + "]";
	}
	
	
}
