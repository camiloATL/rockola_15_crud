package com.app.web.entidades;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="Usuarios")
public class Usuario {
		//Atributos
	@Id //Se pone @Id para un atributo primary key
		//Y @GeneratedValue para hacer el incremento automatico
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	private int idUsuario;
	
	@Column(name="cedulaUsuario", nullable = true, length = 25)
	private long cedulaUsuario;
	
	@Column(name="nombreUsuario", nullable = true, length = 50)
	private String nombreUsuario;
	
	@Column(name="apellidoUsuario", nullable = true, length = 50)
	private String apellidoUsuario;
	
	@Column(name="edadUsuario", nullable = true, length = 3)
	private int edadUsuario;
	
	@Column(name="correoUsuario", nullable = true, length = 50)
	private String correoUsuario;
	
	@Column(name="contraseña", nullable = true, length = 50)
	private String contrasenaUsuario;
	
	@OneToMany(mappedBy="oyeUsuario")
	private Set<Escuchar> escucharUsuario;

	public Usuario() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Usuario(int idUsuario, long cedulaUsuario, String nombreUsuario, String apellidoUsuario, int edadUsuario,
			String correoUsuario, String contrasenaUsuario, Set<Escuchar> escucharUsuario) {
		super();
		this.idUsuario = idUsuario;
		this.cedulaUsuario = cedulaUsuario;
		this.nombreUsuario = nombreUsuario;
		this.apellidoUsuario = apellidoUsuario;
		this.edadUsuario = edadUsuario;
		this.correoUsuario = correoUsuario;
		this.contrasenaUsuario = contrasenaUsuario;
		this.escucharUsuario = escucharUsuario;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public long getCedulaUsuario() {
		return cedulaUsuario;
	}

	public void setCedulaUsuario(long cedulaUsuario) {
		this.cedulaUsuario = cedulaUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getApellidoUsuario() {
		return apellidoUsuario;
	}

	public void setApellidoUsuario(String apellidoUsuario) {
		this.apellidoUsuario = apellidoUsuario;
	}

	public int getEdadUsuario() {
		return edadUsuario;
	}

	public void setEdadUsuario(int edadUsuario) {
		this.edadUsuario = edadUsuario;
	}

	public String getCorreoUsuario() {
		return correoUsuario;
	}

	public void setCorreoUsuario(String correoUsuario) {
		this.correoUsuario = correoUsuario;
	}

	public String getContrasenaUsuario() {
		return contrasenaUsuario;
	}

	public void setContrasenaUsuario(String contrasenaUsuario) {
		this.contrasenaUsuario = contrasenaUsuario;
	}

	public Set<Escuchar> getEscucharUsuario() {
		return escucharUsuario;
	}

	public void setEscucharUsuario(Set<Escuchar> escucharUsuario) {
		this.escucharUsuario = escucharUsuario;
	}

	@Override
	public String toString() {
		return "Usuario [idUsuario=" + idUsuario + ", cedulaUsuario=" + cedulaUsuario + ", nombreUsuario="
				+ nombreUsuario + ", apellidoUsuario=" + apellidoUsuario + ", edadUsuario=" + edadUsuario
				+ ", correoUsuario=" + correoUsuario + ", contrasenaUsuario=" + contrasenaUsuario + ", escucharUsuario="
				+ escucharUsuario + "]";
	}
	
	
}
