package com.app.web.servicios;

import java.util.List;

import com.app.web.entidades.Usuario;

public interface UsuarioServicio {

	public List<Usuario> listarUsuarios();
	
	public Usuario guardarUsuario(Usuario objUsuario);
	
	public Usuario usuarioXid(int id);

	public Usuario actualizarUsuario(Usuario objUsuario);
	
	public void eliminarUsuario(int id);
	
	public void mostrarHome();
}
