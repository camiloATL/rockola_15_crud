package com.app.web.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.web.entidades.Usuario;
import com.app.web.repositorios.UsuarioRepositorio;

@Service
public class UsuarioServicioImpl 
		implements UsuarioServicio{

	@Autowired
	public UsuarioRepositorio repositorio;
	
	@Override
	public List<Usuario> listarUsuarios() {
		// TODO Auto-generated method stub
		return repositorio.findAll();
	}

	@Override
	public Usuario guardarUsuario(Usuario objUsuario) {
		return repositorio.save(objUsuario);
	}

	@Override
	public Usuario usuarioXid(int id) {
		return repositorio.findById(id).get();
	}

	@Override
	public Usuario actualizarUsuario(Usuario objUsuario) {
		return repositorio.save(objUsuario);
	}

	@Override
	public void eliminarUsuario(int id) {
		repositorio.deleteById(id);
		
	}

	@Override
	public void mostrarHome() {
		
	}
	
}


